import { Component, OnInit } from '@angular/core';


//servicio
import {ClienteService} from '../../../services/cliente.service';
import { Cliente } from 'src/app/models/cliente';

@Component({
  selector: 'app-clientes-lista',
  templateUrl: './clientes-lista.component.html',
  styleUrls: ['./clientes-lista.component.css']
})
export class ClientesListaComponent implements OnInit {

  clienteLista: Cliente[];

  
  constructor(private clienteService: ClienteService) { }

  ngOnInit() {
    this.clienteService.getClientes()
    .snapshotChanges()
    .subscribe(item => {
      this.clienteLista = [];
      item.forEach(element => {
       let X =  element.payload.toJSON();
       X["$key"] = element.key;
       this.clienteLista.push(X as Cliente);
        
      });

    });

  }

  onEdit(cliente: Cliente){
    this.clienteService.selectCliente = Object.assign({}, cliente);
  }

  onDelete($key: string){
   
    
      this.clienteService.deleteCliente($key);
     
    
    
  }

}

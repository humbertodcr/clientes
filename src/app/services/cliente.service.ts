import { Injectable } from '@angular/core';
import { AngularFireDatabase, AngularFireList } from 'angularfire2/database';
import {Cliente} from '../models/cliente';

@Injectable({
  providedIn: 'root'
})
export class ClienteService {


  clienteLista: AngularFireList<any>;
  selectCliente: Cliente = new Cliente();

  constructor(private firebase: AngularFireDatabase) { }

  getClientes()
  {
    return this.clienteLista = this.firebase.list('clientes');
  }


  
  insertCliente(cliente: Cliente)
  {
    this.clienteLista.push({
      cedula: cliente.cedula,
      nombre: cliente.nombre,
      apellido: cliente.apellido,
      localidad: cliente.localidad,
      telefono: cliente.telefono,
    });
  }

  updateCliente(cliente: Cliente){
    this.clienteLista.update(cliente.$key, {
      cedula: cliente.cedula,
      nombre: cliente.nombre,
      apellido: cliente.apellido,
      localidad: cliente.localidad,
      telefono: cliente.telefono,
    });
  }

  deleteCliente($key: string){
    this.clienteLista.remove($key);
  }
}
